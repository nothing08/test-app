#!/usr/bin/env bash
#Place this script in project/ios/
#ADD azure fingerprint to known_hosts
mkdir -p ~/.ssh
echo "Adding azure to ssh known hosts"
ssh-keyscan -t rsa ssh.dev.azure.com >> ~/.ssh/known_hosts
#ADD SSH key to the image
echo "Adding azure SSH key"
echo $AZURE_SSH_KEY | base64 -D > ~/.ssh/id_rsa
chmod 600 ~/.ssh/id_rsa
ssh-add ~/.ssh/id_rsa
echo "Add azure SSH key success!"

cd ..

# fail if any command fails
set -e
# debug log
set -x

cd ..
git clone --branch 3.7.10 https://github.com/flutter/flutter.git
export PATH=`pwd`/flutter/bin:$PATH
echo "Installed flutter to `pwd`/flutter"
flutter doctor

cd s
dart pub global activate rename
dart pub global run rename --bundleId "com.hiteam.coffeehrs"
#dart pub global run rename --appname $CONFIG_APP_NAME
#echo $CONFIG_APP | base64 -D > $APPCENTER_SOURCE_DIRECTORY/assets/cfg/app_config.json
#echo $CONFIG_FIREBASE | base64 -D > $APPCENTER_SOURCE_DIRECTORY/ios/Runner/GoogleService-Info.plist
flutter pub get
flutter build ios --release --no-codesign
